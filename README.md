
# Domain Chain Library

## Objetivo 

Domain Chain é uma biblioteca que facilita recuperar e salvar informações 
em uma cadeia de chamadas de consulta, a partir, de encapsulamento de estrutura de dados para encadear chamadas 
em interfaces polimorficas. Dessa forma as camadas de domínio, aplicação e adaptadores do sistema cliente da biblioteca,
continuaram a utilizar suas interfaces para cruzar limites arquiteturais sem qualquer impacto. 
 
Domain chain foi desenhado pensando no design pattern de responsability chain, para recuperar informações de um dominio. As classes
adaptadores diferentemente do padrão não utilizaram herança e sim composição.

## Class Diagram

![Diagrama](image/diagrama.jpg)