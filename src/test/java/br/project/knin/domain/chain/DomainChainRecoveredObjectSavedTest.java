package br.project.knin.domain.chain;

import br.project.knin.domain.DomainGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

class DomainChainRecoveredObjectSavedTest {

    @Test
    void shouldReturnAObjectAndNotSave() {

        final DomainGateway<String, Long> domainGateway =
                DomainGateway
                        .create
                                (
                                        s -> Optional.of(Long.valueOf(s)),
                                        valueLong -> {
                                            throw new IllegalAccessError("Should not be here!");
                                        }
                                );

        final DomainChainAbstract<String, Long> domainChainAbstract =
                new DomainChainRecoveredObjectSaved<>(null, domainGateway);

        final Optional<Long> optional = domainChainAbstract.recover("224732442372");

        Assertions.assertTrue(optional.isPresent());

        final Long value = optional.get();

        Assertions.assertEquals(224732442372L, value);

    }

    @Test
    void shouldReturnAObjectAndSave() {

        final DomainGateway<Long, Long> gateway = DomainGateway
                .create
                        (
                                Optional::of,
                                aDouble -> {
                                    throw new IllegalAccessError("Should not be here!");
                                }
                        );

        final DomainChainAbstract<Long, Long> next =
                new DomainChainRecoveredObjectSaved<>(null, gateway);

        final AtomicLong valueSaved = new AtomicLong(0L);

        final DomainGateway<Long, Long> domainGateway =
                DomainGateway
                        .create
                                (
                                        s -> Optional.empty(),
                                        valueSaved::set
                                );

        final DomainChainAbstract<Long, Long> domainChainAbstract =
                new DomainChainRecoveredObjectSaved<>(next, domainGateway);

        final long expected = 224732442374L;

        final Optional<Long> optional = domainChainAbstract.recover(expected);

        Assertions.assertTrue(optional.isPresent());

        final Long aLong = optional.get();

        Assertions.assertEquals(expected, aLong);

        Assertions.assertEquals(valueSaved.get(), aLong);

    }

    @Test
    void whenDomainQueryIsNullShouldNotReturnAObject() {

        final DomainGateway<BigDecimal, BigDecimal> domainGateway =
                DomainGateway
                        .create
                                (
                                        null,
                                        bigDecimal -> {
                                        }
                                );

        final DomainChainAbstract<BigDecimal, BigDecimal> domainChain =
                new DomainChainRecoveredObjectSaved<>(null, domainGateway);

        final Optional<BigDecimal> bigDecimal = domainChain.recover(BigDecimal.ZERO);

        Assertions.assertTrue(bigDecimal.isEmpty());

    }

    @Test
    void whenDomainCommandIsNullShouldThrowAException() {

        final DomainGateway<BigDecimal, BigDecimal> domainGatewayRecovered =

                DomainGateway
                        .create
                                (
                                        Optional::of,
                                        bigDecimal -> {
                                            throw new IllegalAccessError();
                                        }
                                );

        final DomainChainAbstract<BigDecimal, BigDecimal> domainChainAbstract =
                new DomainChainRecoveredObjectSaved<>(null, domainGatewayRecovered);

        final DomainGateway<BigDecimal, BigDecimal> domainGateway =
                DomainGateway
                        .create
                                (
                                        null,
                                        null
                                );

        final DomainChainAbstract<BigDecimal, BigDecimal> domainChainRecoveredObjectSaved =
                new DomainChainRecoveredObjectSaved<>(domainChainAbstract, domainGateway);

        Assertions
                .assertThrows
                        (
                                IllegalCallerException.class,
                                () -> domainChainRecoveredObjectSaved.recover(BigDecimal.ZERO)
                        );

    }

    @Test
    void whenDomainGatewayIsNullShouldNotReturnAObject() {

        final DomainChainAbstract<Boolean, Integer> domainChainRecoveredObjectSaved =
                new DomainChainRecoveredObjectSaved<>(null, null);

        final Optional<Integer> optional = domainChainRecoveredObjectSaved.recover(true);

        Assertions.assertTrue(optional.isEmpty());

    }

}