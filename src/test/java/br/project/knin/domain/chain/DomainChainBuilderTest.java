package br.project.knin.domain.chain;

import br.project.knin.domain.DomainCommand;
import br.project.knin.domain.DomainGateway;
import br.project.knin.domain.DomainQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

class DomainChainBuilderTest {

    @Test
    void shouldCreateQueryChainWithOnlyOneDomainQuery() {

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> Optional.of(String.valueOf(integer)))
                        .build();

        final Optional<String> optional = domainQuery.recover(2);

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertEquals("2", optional.get());

    }

    @Test
    void shouldCreateQueryChainWithTwoDomainQueryInNaturalOrder() {

        final AtomicInteger atomicInteger = new AtomicInteger(1);

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> {
                            atomicInteger.set(atomicInteger.get() + atomicInteger.get());
                            return Optional.empty();
                        })
                        .add(integer -> {
                            atomicInteger.set(atomicInteger.get() * atomicInteger.get());
                            return Optional.empty();
                        })
                        .build();

        final Optional<String> optional = domainQuery.recover(-1);

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertEquals(4, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithTwoDomainQueryInReverseOrder() {

        final AtomicInteger atomicInteger = new AtomicInteger(1);

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> {
                            atomicInteger.set(atomicInteger.get() + atomicInteger.get());
                            return Optional.empty();
                        })
                        .add(integer -> {
                            atomicInteger.set(atomicInteger.get() * atomicInteger.get());
                            return Optional.empty();
                        })
                        .build(DomainChainBuilder.DomainChainType.REVERSE_ORDER);

        final Optional<String> optional = domainQuery.recover(0);

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertEquals(2, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithFourDomainChainAbstractInNaturalOrder() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        final DomainQuery<Integer, Map<Integer, String>> domainQuery =
                DomainChainBuilder
                        .<Integer, Map<Integer, String>>builder()
                        .add(integer -> Optional.empty(), m -> atomicInteger.set(atomicInteger.get() + atomicInteger.get()))
                        .add(integer -> Optional.empty(), m -> atomicInteger.set(atomicInteger.get() * atomicInteger.get()))
                        .add(integer -> Optional.empty(), m -> atomicInteger.addAndGet(10))
                        .add(integer -> Optional.of(Map.of(integer, String.valueOf(integer))))
                        .build();

        final Optional<Map<Integer, String>> optional = domainQuery.recover(0);

        Assertions.assertTrue(optional.isPresent());

        final Map<Integer, String> integerStringMap = optional.get();

        Assertions.assertEquals(1, integerStringMap.size());

        final String string = integerStringMap.get(0);

        Assertions.assertNotNull(string);

        Assertions.assertEquals("0", string);

        Assertions.assertEquals(200, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithFourDomainChainAbstractInNaturalOrderButWithoutReturnAObject() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> Optional.empty(), s -> atomicInteger.set(atomicInteger.get() + atomicInteger.get()))
                        .add(integer -> Optional.empty(), s -> atomicInteger.set(atomicInteger.get() * atomicInteger.get()))
                        .add(integer -> Optional.empty(), s -> atomicInteger.addAndGet(10))
                        .add(integer -> Optional.empty())
                        .build();

        final Optional<String> optional = domainQuery.recover(0);

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertEquals(0, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithFourDomainChainAbstractInReverseOrderButWithoutReturnAObject() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> Optional.empty(), s -> atomicInteger.set(atomicInteger.get() + atomicInteger.get()))
                        .add(integer -> Optional.empty(), s -> atomicInteger.set(atomicInteger.get() * atomicInteger.get()))
                        .add(integer -> Optional.empty(), s -> atomicInteger.addAndGet(10))
                        .add(integer -> Optional.empty())
                        .build(DomainChainBuilder.DomainChainType.REVERSE_ORDER);

        final Optional<String> optional = domainQuery.recover(0);

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertEquals(0, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithFourDomainChainAbstractInReverse() {

        final AtomicInteger atomicInteger = new AtomicInteger(0);

        final DomainQuery<Integer, String> domainQuery =
                DomainChainBuilder
                        .<Integer, String>builder()
                        .add(integer -> {
                            throw new IllegalCallerException();
                        }, null)
                        .add(integer -> {
                            throw new IllegalCallerException();
                        })
                        .add(integer -> {
                            throw new IllegalCallerException();
                        }, null)
                        .add(integer -> {
                            throw new IllegalCallerException();
                        }, null)
                        .add(integer -> Optional.of(String.valueOf(-10)))
                        .build(DomainChainBuilder.DomainChainType.REVERSE_ORDER);

        final Optional<String> optional = domainQuery.recover(0);

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertEquals("-10", optional.get());

        Assertions.assertEquals(0, atomicInteger.get());

    }

    @Test
    void whenABuildTryAddANullDomainQueryShouldThrowsNullPointerException() {

        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> DomainChainBuilder.<Integer, Integer>builder().add(null)
                );

    }

    @Test
    void whenABuildTryAddANullDomainGatewayShouldThrowsNullPointerException() {

        Assertions.assertThrows
                (
                        NullPointerException.class,
                        () -> DomainChainBuilder.<Integer, Integer>builder().add((DomainQuery<Integer, Integer>) null)
                );

    }

    @Test
    void shouldCreateQueryChainWithTwoDomainGatewayInstanceInNaturalOrder() {

        final AtomicInteger atomicInteger = new AtomicInteger(1);

        final DomainQuery<String, String> domainQuery = DomainChainBuilder
                .<String, String>
                        builder()
                .add
                        (
                                DomainGateway.create
                                        (
                                                s ->
                                                {
                                                    atomicInteger.incrementAndGet();
                                                    return Optional.empty();
                                                },
                                                DomainCommand.nullable()
                                        )
                        )
                .add
                        (
                                DomainGateway.create
                                        (
                                                s -> {
                                                    atomicInteger.set(atomicInteger.get() * atomicInteger.get());
                                                    return Optional.empty();
                                                },
                                                DomainCommand.nullable()
                                        )
                        )
                .build();

        Assertions.assertTrue(domainQuery.recover("2").isEmpty());

        Assertions.assertEquals(4, atomicInteger.get());

    }

    @Test
    void shouldCreateQueryChainWithTwoDomainGatewayInstanceInReverseOrder() {

        final AtomicInteger atomicInteger = new AtomicInteger(1);

        final DomainQuery<String, String> domainQuery = DomainChainBuilder
                .<String, String>
                        builder()
                .add
                        (
                                DomainGateway.create
                                        (
                                                s ->
                                                {
                                                    atomicInteger.incrementAndGet();
                                                    return Optional.empty();
                                                },
                                                DomainCommand.nullable()
                                        )
                        )
                .add
                        (
                                DomainGateway.create
                                        (
                                                s -> {
                                                    atomicInteger.set(atomicInteger.get() * atomicInteger.get());
                                                    return Optional.empty();
                                                },
                                                DomainCommand.nullable()
                                        )
                        )
                .build(DomainChainBuilder.DomainChainType.REVERSE_ORDER);

        Assertions.assertTrue(domainQuery.recover("2").isEmpty());

        Assertions.assertEquals(2, atomicInteger.get());

    }

}