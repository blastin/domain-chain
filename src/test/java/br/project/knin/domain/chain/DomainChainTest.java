package br.project.knin.domain.chain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class DomainChainTest {

    @Test
    void shouldReturnAObject() {

        final DomainChainAbstract<Character, String> domainChainAbstract =
                new DomainChain<>
                        (
                                null,
                                character -> Optional.ofNullable(String.valueOf(character))
                        );

        final Optional<String> optional = domainChainAbstract.recover('F');

        Assertions.assertTrue(optional.isPresent());

        final String string = optional.get();

        Assertions.assertEquals("F", string);

    }

    @Test
    void shouldNotReturnAObject() {

        final DomainChainAbstract<Float, Double> domainChainAbstract =
                new DomainChain<>
                        (
                                null,
                                floatValue -> Optional.empty()
                        );

        final Optional<Double> optional = domainChainAbstract.recover(1.2f);

        Assertions.assertTrue(optional.isEmpty());

    }

    @Test
    void whenDomainQueryIsNullShouldNotReturnAObject() {

        final DomainChainAbstract<Integer, Integer> domainChainAbstract = new DomainChain<>(null, null);

        final Optional<Integer> optional = domainChainAbstract.recover(1);

        Assertions.assertTrue(optional.isEmpty());

    }
}