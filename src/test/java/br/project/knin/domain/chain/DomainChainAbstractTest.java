package br.project.knin.domain.chain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

class DomainChainAbstractTest {

    @Test
    void shouldReturnAObject() {

        final DomainChainAbstract<Integer, String> domainChainAbstract = new DomainChainAbstract<>(null) {
            @Override
            protected Optional<String> item(final Integer integer) {
                return Optional.of(integer.toString());
            }
        };

        final int item = 1;

        final Optional<String> optional = domainChainAbstract.recover(item);

        Assertions.assertTrue(optional.isPresent());

        final String itemString = optional.get();

        Assertions.assertEquals(String.valueOf(item), itemString);

    }

    @Test
    void shouldNotReturnAObjectBecauseItNotFound() {

        final DomainChainAbstract<String, BigDecimal> domainChainAbstract = new DomainChainAbstract<>(null) {
            @Override
            protected Optional<BigDecimal> item(final String s) {
                return Optional.empty();
            }
        };

        final Optional<BigDecimal> optional = domainChainAbstract.recover("3.14");

        Assertions.assertFalse(optional.isPresent());

    }

    @Test
    void nextShouldReturnAObject() {

        final DomainChainAbstract<Integer, Integer> next = new DomainChainAbstract<>(null) {
            @Override
            protected Optional<Integer> item(final Integer integer) {
                return Optional.of(integer);
            }
        };

        final DomainChainAbstract<Integer, Integer> domainChainAbstract = new DomainChainAbstract<>(next) {
            @Override
            protected Optional<Integer> item(final Integer s) {
                return Optional.empty();
            }
        };

        final int expected = 100;

        final Optional<Integer> optional = domainChainAbstract.recover(expected);

        Assertions.assertTrue(optional.isPresent());

        final Integer integer = optional.get();

        Assertions.assertEquals(expected, integer);

    }

    @Test
    void nextShouldNotReturnAObject() {

        final DomainChainAbstract<Integer, Integer> next = new DomainChainAbstract<>(null) {
            @Override
            protected Optional<Integer> item(final Integer integer) {
                return Optional.empty();
            }
        };

        final DomainChainAbstract<Integer, Integer> domainChainAbstract = new DomainChainAbstract<>(next) {
            @Override
            protected Optional<Integer> item(final Integer s) {
                return Optional.empty();
            }
        };

        final Optional<Integer> optional = domainChainAbstract.recover(-200);

        Assertions.assertTrue(optional.isEmpty());

    }

    @Test
    void nextShouldNotBeCalled() {

        final DomainChainAbstract<Integer, Integer> next = new DomainChainAbstract<>(null) {
            @Override
            protected Optional<Integer> item(final Integer integer) {
                throw new IllegalCallerException("Should not be here");
            }
        };

        final DomainChainAbstract<Integer, Integer> domainChainAbstract = new DomainChainAbstract<>(next) {
            @Override
            protected Optional<Integer> item(final Integer s) {
                return Optional.of(s);
            }
        };

        final int expected = -200;

        final Optional<Integer> optional = domainChainAbstract.recover(expected);

        Assertions.assertTrue(optional.isPresent());

        final Integer integer = optional.get();

        Assertions.assertEquals(expected, integer);

    }
}