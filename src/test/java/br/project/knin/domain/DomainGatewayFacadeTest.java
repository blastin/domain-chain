package br.project.knin.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class DomainGatewayFacadeTest {

    @Test
    void shouldNotReturnAObject() {

        final DomainGateway<Float, Double> floatDoubleDomainGateway =
                new DomainGateway.DomainGatewayFacade<>
                        (
                                aFloat -> Optional.empty(),
                                aDouble -> {
                                }
                        );

        final Optional<Double> optional = floatDoubleDomainGateway.recover(1F);

        Assertions.assertTrue(optional.isEmpty());

    }

    @Test
    void shouldNotReturnAObjectBecauseInstanceOfDomainQueryIsEqualsNull() {

        final DomainGateway<Float, Double> floatDoubleDomainGateway =
                new DomainGateway.DomainGatewayFacade<>
                        (
                                null,
                                null
                        );

        final Optional<Double> optional = floatDoubleDomainGateway.recover(1F);

        Assertions.assertTrue(optional.isEmpty());

    }

    @Test
    void shouldNotThrowAExceptionBecauseInstanceOfDomainCommandIsNotEqualsNull() {

        final DomainGateway<Float, Double> floatDoubleDomainGateway =
                new DomainGateway.DomainGatewayFacade<>
                        (
                                null,
                                aDouble -> {
                                }
                        );


        Assertions.assertDoesNotThrow(() -> floatDoubleDomainGateway.save(2.0));

    }

    @Test
    void shouldThrowAExceptionBecauseInstanceOfDomainCommandIsEqualsNull() {

        final DomainGateway<Float, Double> floatDoubleDomainGateway =
                new DomainGateway.DomainGatewayFacade<>
                        (
                                aFloat -> Optional.of(Double.valueOf(aFloat)),
                                null
                        );

        Assertions.assertThrows
                (
                        IllegalCallerException.class,
                        () -> floatDoubleDomainGateway.save(2.0)
                );

    }
}