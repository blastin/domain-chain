package br.project.knin.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class DomainQueryTest {

    @Test
    void shouldReturnOptionalEmpty() {

        final DomainQuery<Integer, Integer> domainQuery = DomainQuery.nullable();

        Assertions.assertTrue(domainQuery.recover(1).isEmpty());

    }

    @Test
    void shouldReturnCollectionOfIntegers() {

        final List<Integer> numbers = List.of(1, 2, 3);

        final DomainQuery<Integer, Collection<Integer>> domainQuery = integer -> Optional.of(numbers);

        final Collection<Integer> integers = DomainQuery.recoverToCollection(1, domainQuery);

        Assertions.assertFalse(integers.isEmpty());

        Assertions.assertEquals(numbers, integers);

    }

    @Test
    void shouldReturnCollectionEmpty() {

        final DomainQuery<Integer, Collection<Integer>> domainQuery = integer -> Optional.empty();

        final Collection<Integer> integers = DomainQuery.recoverToCollection(1, domainQuery);

        Assertions.assertTrue(integers.isEmpty());

    }

    @Test
    void shouldReturnMapOfValuesString() {

        final Map<Integer, String> values = Map.of(1, "Matrix", 2, "Lord of the rings");

        final DomainQuery<Integer, Map<Integer, String>> domainQuery = integer -> Optional.of(values);

        final Map<Integer, String> mapRecover = DomainQuery.recoverToMap(1, domainQuery);

        Assertions.assertFalse(mapRecover.isEmpty());

        Assertions.assertEquals(values, mapRecover);

    }

    @Test
    void shouldReturnMapEmpty() {

        final DomainQuery<Integer, Map<Integer, String>> domainQuery = integer -> Optional.empty();

        final Map<Integer, String> mapRecover = DomainQuery.recoverToMap(1, domainQuery);

        Assertions.assertTrue(mapRecover.isEmpty());

    }

}