package br.project.knin.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * @param <E> Entry
 * @param <S> Object of return
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface DomainQuery<E, S> {

    static <E, S> DomainQuery<E, S> nullable() {
        return e -> Optional.empty();
    }

    static <T, E, S extends Collection<T>> Collection<T> recoverToCollection
            (
                    final E e, final DomainQuery<? super E, S> domainQuery
            ) {

        final Optional<S> optional = domainQuery.recover(e);

        return optional.orElse(castCollectionEmpty());

    }

    static <A, B, E, S extends Map<A, B>> Map<A, B> recoverToMap
            (
                    final E e, final DomainQuery<? super E, S> domainQuery
            ) {

        final Optional<S> optional = domainQuery.recover(e);

        return optional.orElse(castMapEmpty());

    }

    @SuppressWarnings("unchecked")
    private static <T, S extends Collection<T>> S castCollectionEmpty() {
        return (S) Collections.emptyList();
    }

    @SuppressWarnings("unchecked")
    private static <A, B, S extends Map<A, B>> S castMapEmpty() {
        return (S) Map.of();
    }

    /**
     * @param e parameter of E generic type
     * @return Possibility of recover object type S
     */
    Optional<S> recover(final E e);

}
