package br.project.knin.domain;

import java.util.Optional;

/**
 * @param <E> Type of entry object
 * @param <S> Type of recover object
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface DomainGateway<E, S> extends DomainQuery<E, S>, DomainCommand<S> {

    static <E, S> DomainGateway<E, S> nullable() {
        return create(null, null);
    }

    static <E, S> DomainGateway<E, S> create(final DomainQuery<? super E, S> domainQuery, final DomainCommand<S> domainCommand) {
        return new DomainGatewayFacade<>(domainQuery, domainCommand);
    }

    /**
     * @param <E>
     * @param <S>
     */
    final class DomainGatewayFacade<E, S> implements DomainGateway<E, S> {

        DomainGatewayFacade(final DomainQuery<? super E, S> domainQuery, final DomainCommand<S> domainCommand) {
            this.domainQuery = domainQuery == null ? DomainQuery.nullable() : domainQuery;
            this.domainCommand = domainCommand == null ? DomainCommand.nullable() : domainCommand;
        }

        private final DomainQuery<? super E, S> domainQuery;

        private final DomainCommand<S> domainCommand;

        @Override
        public void save(final S s) {
            domainCommand.save(s);
        }

        @Override
        public Optional<S> recover(final E e) {
            return domainQuery.recover(e);
        }

    }

}
