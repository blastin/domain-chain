package br.project.knin.domain;

/**
 * @param <S> entry object of generic E type
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface DomainCommand<S> {

    static <S> DomainCommand<S> nullable() {
        return s -> {
            throw new IllegalCallerException("You should not be here");
        };
    }

    /**
     * This contract makes it easy to save an object
     *
     * @param s object to save
     */
    void save(final S s);

}
