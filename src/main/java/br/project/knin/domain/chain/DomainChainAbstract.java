package br.project.knin.domain.chain;

import br.project.knin.domain.DomainQuery;

import java.util.Optional;

/**
 * @param <E>
 * @param <S>
 */
abstract class DomainChainAbstract<E, S> implements DomainQuery<E, S> {

    protected DomainChainAbstract(final DomainChainAbstract<? super E, S> next) {
        this.next = next;
    }

    private final DomainChainAbstract<? super E, S> next;

    @Override
    public final Optional<S> recover(final E e) {
        return Optional
                .ofNullable
                        (
                                item(e)
                                        .orElseGet
                                                (
                                                        () ->
                                                                callNext(e)
                                                                        .orElse(null)
                                                )
                        );
    }

    Optional<S> callNext(final E e) {
        if (next == null) return Optional.empty();
        return next.recover(e);
    }

    protected abstract Optional<S> item(final E e);

}
