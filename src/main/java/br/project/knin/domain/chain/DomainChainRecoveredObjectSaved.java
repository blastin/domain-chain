package br.project.knin.domain.chain;

import br.project.knin.domain.DomainGateway;

import java.util.Optional;

/**
 * @param <E>
 * @param <S>
 */
final class DomainChainRecoveredObjectSaved<E, S> extends DomainChainAbstract<E, S> {

    DomainChainRecoveredObjectSaved(final DomainChainAbstract<? super E, S> next, final DomainGateway<? super E, S> domainGateway) {
        super(next);
        this.domainGateway = domainGateway == null ? DomainGateway.nullable() : domainGateway;
    }

    private final DomainGateway<? super E, S> domainGateway;

    @Override
    protected Optional<S> item(final E e) {
        return domainGateway.recover(e);
    }

    @Override
    Optional<S> callNext(final E e) {

        final Optional<S> optional = super.callNext(e);

        optional.ifPresent(domainGateway::save);

        return optional;

    }

}
