package br.project.knin.domain.chain;

import br.project.knin.domain.DomainCommand;
import br.project.knin.domain.DomainGateway;
import br.project.knin.domain.DomainQuery;

/**
 * @param <E>
 * @param <S>
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface DomainChainBuilder<E, S> {

    static <E, S> DomainChainBuilder<E, S> builder() {
        return new DomainChainBuilderAbstract<>();
    }

    DomainChainBuilder<E, S> add(final DomainQuery<E, S> domainQuery);

    DomainChainBuilder<E, S> add(final DomainQuery<E, S> domainQuery, final DomainCommand<S> domainCommand);

    DomainChainBuilder<E, S> add(final DomainGateway<E, S> domainGateway);

    DomainQuery<E, S> build();

    enum DomainChainType {
        NATURAL_ORDER,
        REVERSE_ORDER
    }

    DomainQuery<E, S> build(final DomainChainType domainChainType);

}
