package br.project.knin.domain.chain;

import br.project.knin.domain.DomainCommand;
import br.project.knin.domain.DomainGateway;
import br.project.knin.domain.DomainQuery;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

/**
 * @param <E>
 * @param <S>
 */
final class DomainChainBuilderAbstract<E, S> implements DomainChainBuilder<E, S> {

    enum DomainType {
        QUERY,
        RECOVERY_SAVE
    }

    private static <E, S> DomainChainAbstract<E, S> queryCreate
            (
                    final DomainChainAbstract<E, S> next,
                    final Deque<DomainQuery<E, S>> queries,
                    final DomainChainType domainChainType
            ) {

        final DomainQuery<E, S> domain = switch (domainChainType) {
            case NATURAL_ORDER -> queries.removeLast();
            case REVERSE_ORDER -> queries.removeFirst();
        };

        return new DomainChain<>(next, domain);

    }

    private static <E, S> DomainChainAbstract<E, S> gatewayCreate
            (
                    final DomainChainAbstract<E, S> next,
                    final Deque<DomainGateway<E, S>> gateways,
                    final DomainChainType domainChainType
            ) {

        final DomainGateway<E, S> gateway = switch (domainChainType) {
            case NATURAL_ORDER -> gateways.removeLast();
            case REVERSE_ORDER -> gateways.removeFirst();
        };

        return new DomainChainRecoveredObjectSaved<>(next, gateway);

    }

    DomainChainBuilderAbstract() {
        gateways = new ArrayDeque<>();
        queries = new ArrayDeque<>();
        types = new ArrayDeque<>();
    }

    private final Deque<DomainGateway<E, S>> gateways;

    private final Deque<DomainQuery<E, S>> queries;

    private final Deque<DomainType> types;

    @Override
    public DomainChainBuilder<E, S> add(final DomainQuery<E, S> domainQuery) {

        Objects.requireNonNull(domainQuery);

        queries.add(domainQuery);

        types.add(DomainType.QUERY);

        return this;

    }

    @Override
    public DomainChainBuilder<E, S> add(final DomainQuery<E, S> domainQuery, final DomainCommand<S> domainCommand) {

        final DomainGateway<E, S> domainGateway = DomainGateway.create(domainQuery, domainCommand);

        return add(domainGateway);

    }

    @Override
    public DomainChainBuilder<E, S> add(final DomainGateway<E, S> domainGateway) {

        Objects.requireNonNull(domainGateway);

        gateways.add(domainGateway);

        types.add(DomainType.RECOVERY_SAVE);

        return this;

    }

    @Override
    public DomainQuery<E, S> build() {
        return recursive(DomainChainType.NATURAL_ORDER);
    }

    @Override
    public DomainQuery<E, S> build(final DomainChainType domainChainType) {
        return recursive(domainChainType);
    }

    private DomainChainAbstract<E, S> recursive(final DomainChainType domainChainType) {

        if (types.isEmpty()) return null;

        final DomainType domainType = domainChainType
                .equals(DomainChainType.NATURAL_ORDER)
                ? types.removeFirst()
                : types.removeLast();

        final DomainChainAbstract<E, S> next = recursive(domainChainType);

        return switch (domainType) {
            case QUERY -> queryCreate(next, queries, domainChainType);
            case RECOVERY_SAVE -> gatewayCreate(next, gateways, domainChainType);
        };

    }

}

