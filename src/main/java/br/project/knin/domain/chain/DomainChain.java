package br.project.knin.domain.chain;

import br.project.knin.domain.DomainQuery;

import java.util.Optional;

/**
 * @param <E>
 * @param <S>
 */
final class DomainChain<E, S> extends DomainChainAbstract<E, S> {

    DomainChain(final DomainChainAbstract<? super E, S> next, final DomainQuery<? super E, S> domainQuery) {
        super(next);
        this.domainQuery = domainQuery == null ? DomainQuery.nullable() : domainQuery;
    }

    private final DomainQuery<? super E, S> domainQuery;

    @Override
    protected Optional<S> item(final E e) {
        return domainQuery.recover(e);
    }

}
